<?php
/************************************************************
/*  GeoDRW v0.1: Detector and redirecting websites in PHP
/*  Author: Junior Carrillo - Website: JuniorCarrillo.com.ve
/*  Contact: soyjrcarrillo@gmail.com - +58 (246) 432.71.01
************************************************************/

error_reporting(E_ALL & ~E_NOTICE);
$ip = $_SERVER["REMOTE_ADDR"]; // Client IP
$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));

    $domain  = $_SERVER["HTTP_HOST"]; // Server IP/Domain
    $country = $query["country"];
    $code = $query["countryCode"];
    $language = array("es", "en"); // Language of website
    $protocol = "https"; // Protocol of website

$eu_countries = array("Albania", "Andorra", "Austria", "Armenia", "Azerbaijan", "Belarus", "Belgium", "Bosnia and Herzegovina", "Bulgaria", "Croatia", "Cyprus", "Czech Republic", "Denmark", "Estonia", "Finland", "France", "Georgia", "Germany", "Greece", "Hungary", "Iceland", "Ireland", "Italy", "Kosovo", "Latvia", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Malta", "Moldova", "Monaco", "Montenegro", "The Netherlands", "Norway", "Poland", "Portugal", "Romania", "Russia", "San Marino", "Serbia", "Slovakia", "Slovenia", "Spain", "Sweden", "Switzerland", "Turkey", "Ukraine", "United Kingdom", "Vatican City");

$lae_countries = array("Colombia", "México", "Argentina", "Uruguay", "Puerto Rico", "El Salvador", "Honduras", "Nicaragua", "Costa Rica", "Cuba", "Paraguay", "Bolivia", "Guatemala", "Perú", "República Dominicana", "Panamá", "Ecuador");

    switch ($code) {
        case "VE":
            header("Location: $protocol://$code.$domain/");
            break;
        case "PE":
            header("Location: $protocol://$code.$domain/");
            break;
        case "CL":
            header("Location: $protocol://$code.$domain/");
            break;
        default:
            if (in_array($country, $eu_countries)) {
                header("Location: $protocol://int.$domain/$language[1]/");
            } else {
                if (in_array($country, $lae_countries)) {
                    header("Location: $protocol://int.$domain/$language[0]/");
                    } else {
                        header("Location: $protocol://int.$domain/$language[1]/");
                            }
                    }
                break;
            }
            die();
?>