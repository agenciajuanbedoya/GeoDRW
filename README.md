# GeoDRW v0.1: Detector and redirecting websites in PHP
**Author:** Junior Carrillo  ([Website](https://juniorcarrillo.com.ve) - [Facebook](https://www.facebook.com/SoyJrCarrillo) - [Twitter](https://twitter.com/SoyJrCarrillo) - [LinkedIn](https://linkedin.com/int/ImJrCarrillo))

This is a system that allows you to redirect your website to one that adapts to your sales needs.